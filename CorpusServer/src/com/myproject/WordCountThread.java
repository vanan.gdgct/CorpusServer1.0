package com.myproject;

import java.io.*;
import java.util.*;

/*
 * Thread to create a Dictionary map of words in a file
 */
public class WordCountThread extends Thread
{
	String word, filename;
	Hashtable<String, Integer> wordCount; 
	
	public WordCountThread(String filename)
	{
		this.filename = filename;
		this.wordCount = new Hashtable<String, Integer>();
	}
	
	public Hashtable<String, Integer> getDictionary()
	{
		return this.wordCount;
	}
	 
	public static String preProcessWord(String word)
	{
		try {
		int beginIndex = 0;
    	int endIndex = word.length()-1; 
		Character currentChar = word.charAt(beginIndex);
    	while((Character.isLetter(currentChar)==false) && (Character.isDigit(currentChar)==false))
    	{
    		beginIndex++;
    		currentChar = word.charAt(beginIndex);
    	}
    	currentChar = word.charAt(endIndex);
    	while((Character.isLetter(currentChar)==false) && (Character.isDigit(currentChar)==false))
    	{
    		endIndex--;
    		currentChar = word.charAt(endIndex);
    	}
    	return word.substring(beginIndex, endIndex+1); 
		}
		catch(Exception ex)
		{
			//Ignoring invalid words
			return null;
		}
	}
	
	@Override 
	public void run()
	{
		try 
		{
		    BufferedReader in = new BufferedReader(new FileReader(this.filename));
		    String line; 
		    while((line = in.readLine()) != null)
		    {
		    	line = line.trim().toLowerCase();  
            for(String word : line.split(" "))
            {   
            	//if(word.length()==0) continue;
            	
            	word = preProcessWord(word);
                if(word==null) continue;
                
                if(wordCount.containsKey(word)==true)
                { 
                	wordCount.put(word, wordCount.get(word)+1);
                }
                else
                {
                	wordCount.put(word, 1);
                } 
            }
		   } 
		}catch(Exception ex)
		{
	      System.out.println("Exception Caught: " +ex.toString());		  
		} 
	}
}