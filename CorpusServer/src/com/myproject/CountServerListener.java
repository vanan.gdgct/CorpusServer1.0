package com.myproject;
 
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener; 
import java.util.*;
import java.io.*;

public class CountServerListener implements ServletContextListener{
    
	public String hitsFilePath;
	public static int errorCode;
	
	static
	{
		errorCode = 0;
	}
	
	public void setupPath()
	{
		 String wholePath =  CountServerListener.class.getResource("").getPath(); 
		 String projectPath = wholePath.substring(0, wholePath.indexOf("classes")); 
		 File hitsFile = new File(projectPath + "/corpus/");
		 hitsFilePath = hitsFile.getAbsolutePath() + "/";
	}
	
	public void recordHits(Hashtable<String, Integer> hitsTable)
	{
		  String oldName = "wordHits.txt";
	      String newName = "wordHitsNew.txt";
	      BufferedReader br = null;
	      BufferedWriter bw = null;
	      
	      try { 
	         br = new BufferedReader(new FileReader(hitsFilePath + oldName));
	         bw = new BufferedWriter(new FileWriter(hitsFilePath + newName));
	         String line = br.readLine();
	         while(line != null)
	         {
	        	 String[] wordPair = line.trim().split("-"); 
	        	 if(hitsTable.containsKey(wordPair[0]))
	        	 {
	        		 line  = line.replace(wordPair[1], hitsTable.get(wordPair[0]).toString());
	        		 hitsTable.remove(wordPair[0]); //Remove the keys as and when they are processed
	        	 }
	        	 bw.write(line);
	        	 bw.write("\n");  
		         line = br.readLine();
	         } 
	      } 
	      catch (Exception e) 
	      {
	          System.out.println("Exception caught while recording hits in the File!" + e.getMessage());
	          errorCode=1;
	          return;
	      } 
	       
	      
	      //Check for left over new entries in the hitsTable and write to the new File
	      try {
	      if(hitsTable.isEmpty() == false)
	      {
	    	  Object[] hitsKeys = hitsTable.keySet().toArray();
	    	  for(Object o :hitsKeys)
	    	  {
	    		  StringBuilder keyValue = new StringBuilder();
	    		  keyValue.append(o.toString());
	    		  keyValue.append("-");
	    		  int val = hitsTable.get(o.toString());
	    		  if((val>=0)==false)
	    			  throw new Exception("Value must be greater than zero");
	    		  keyValue.append(hitsTable.get(o.toString()));
	    		  bw.write(keyValue.toString());
	    		  bw.write("\n");
	    	  }
	      }
	      } catch(Exception ex)
	      {
	    	  System.out.println("Exception caught while trying to write contents of hashTable to file");
	    	  errorCode = 1;
	    	  return;
	      }
	      finally
	      {
	    	  try {
		            if(br != null)
		               br.close();
		         } catch (IOException e)
		         {
		            System.out.println("Exception closing buffered reader!");
		            errorCode=1;
		            return;
		         }
		         try {
		            if(bw != null)
		               bw.close();
		         } catch (IOException e) {
		            System.out.println("Exception closing buffered writer");
		            errorCode=1;
		            return;
		         }
	      }
	        
	      File oldFile = new File(hitsFilePath + oldName);
	      oldFile.delete();  
	      File newFile = new File(hitsFilePath + newName);
	      newFile.renameTo(oldFile);   
	      System.out.println("Hits updated successfully!");
	}
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) { 
		//Function inplace
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Tomcat Server about to terminate, pushing hits to file...");
		Hashtable<String, Integer> hitsTable = CountServlet.getWordHits(); 
		setupPath();
		recordHits(hitsTable); 
	}

}