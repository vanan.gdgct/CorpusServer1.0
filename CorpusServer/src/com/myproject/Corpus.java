package com.myproject;

import java.io.*;
import java.util.*;

public class Corpus {
  
	String corpusPath;
	Hashtable<String, Integer> corpusDict;
	WordCountThread[] wc; 
	int errorProcessing;
	
	public Corpus(String corpusPath)
	{
		this.corpusPath = corpusPath; 
		int wc_idx = 0; 
		File dir = new File(this.corpusPath);
		File[] directoryListing = dir.listFiles();
		wc = new WordCountThread[directoryListing.length]; 
		
		//Initialize threads to process each file in the Corpus
		for(File f :directoryListing)
		 {    
			 wc[wc_idx] = new WordCountThread(f.getAbsolutePath()); 
			 wc_idx = wc_idx+1;
		 }
		
		corpusDict = new Hashtable<String, Integer>();
		errorProcessing = 0;
	}
	
	public WordCountThread[] getThreads()
	{
		return wc;
	}
	
	public Hashtable<String, Integer> getCorpusDict()
	{
		return this.corpusDict;
	}
	
	// To merge hash tables obtained from thread corresponding to each file 
	public Hashtable<String, Integer> mergeHashTables(List<Hashtable<String, Integer>> hashTables)
	{ 
		Hashtable<String, Integer> mergedDict = new Hashtable<String, Integer>();
		if((hashTables.size() == 0) || (hashTables==null))
			return mergedDict;
		
		for(Hashtable<String, Integer> ht : hashTables)
		{ 
			 for(String key : ht.keySet())
			 { 
				 if(mergedDict.containsKey(key))
				 {
					 mergedDict.put(key, mergedDict.get(key)+ht.get(key));
				 }
				 else
				 {
					 mergedDict.put(key, ht.get(key));
				 }
			 }
		} 
		return mergedDict;
	}
	
	public int getErrorCode()
	{
		return this.errorProcessing;
	}
	
	public void processCorpus()
	{ 
		try {
		 int wc_idx = 0;   
		 System.out.println("Processing Corpus");
		 
		 //Start the threads corresponding to all files
		 System.out.print("Starting File Threads.. ");
		 for(WordCountThread wcThread : wc)
		 {
			 wcThread.start();
		 }  
		 System.out.println("Done!");
		 
		 //Create a list of hash tables obtained from file threads
		 System.out.println("Waiting for File Threads to Complete..  ");
		 List<Hashtable<String, Integer>> threadDicts = new ArrayList<Hashtable<String, Integer>>();
		 int thread_id = 1;
		 for(WordCountThread wcThread : wc)
		 { 
			 wcThread.join();  
			 System.out.println("Thread " + thread_id + " Finished..");
			 thread_id += 1;
			 Hashtable<String, Integer> threadDict = wcThread.getDictionary();
			 threadDicts.add(threadDict);   
		 }
		 System.out.println("All Threads Completed!");
		 
		//Merge the hash tables
		System.out.print("Creating the Dictionary...     ");
		corpusDict = mergeHashTables(threadDicts);   
		System.out.println("Done!");
		}catch(Exception ex)
		{
			System.out.println("Exception caught while processing corpus " + ex.toString());
			this.errorProcessing = 1;
		}
	}
}
