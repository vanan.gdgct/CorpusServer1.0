 package com.myproject;

import java.nio.file.*;
import java.nio.file.Files;
import java.io.*;
import java.net.URL;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.myproject.test.CorpusTest; 

 
public class CountServlet extends HttpServlet{
	
	public static Hashtable<String, Integer> wordHits; 
	private  Hashtable<String, Integer> corpusDict;
	private String corpusPath;
	public int errorServlet;
	int total;
	 
	//Function to setCorpus path based on whether its accessed in Servlet or Test Script
	public void setCorpus(String corpusDirectory, boolean inServlet)
	{
		StringBuilder completePath = new StringBuilder();
		if(inServlet==true)
		{
			completePath.append(this.getServletContext().getRealPath("WEB-INF"));
			completePath.append("/corpus/");
		}
		else  // for paths in testing environment
		{
			String wholePath =  CorpusTest.class.getResource("").getPath();
			String projectPath = wholePath.substring(0, wholePath.indexOf("build"));
			File testFolder = new File(projectPath + "/WebContent/WEB-INF/corpus/"); 
			completePath.append(testFolder.getAbsolutePath()); 
			completePath.append("/");
		}
		completePath.append(corpusDirectory);
	    this.corpusPath = completePath.toString(); 
		//ifServlet false case handled in testing with a different Corpus/ way of obtaining the path
	}
	
	static Hashtable<String, Integer> getWordHits()
	{
		return wordHits;
	
	}
	 
	public void initiateProcessing()
	{
		Corpus gutenbergCorpus = new Corpus(corpusPath); 
		gutenbergCorpus.processCorpus();
		corpusDict = gutenbergCorpus.getCorpusDict();
	}
	
	public void populateWordHits(String hitsFile) 
	{
		errorServlet=0;
		wordHits = new Hashtable<String, Integer>();
		try (BufferedReader br = new BufferedReader(new FileReader(hitsFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	System.out.println("Line is: " + line);
		       String wordCountPair[] = line.trim().split("-"); 
		       wordHits.put(wordCountPair[0], Integer.valueOf(wordCountPair[1]));
		    }
		    System.out.println("Hits Table populated!");
		}
		catch(Exception ex)
		{
			System.out.println("Exception occurred while trying to populate hits table from wordHits.txt file");
		    System.out.println(ex.toString()); 
		    errorServlet=1;
		} 
	}
	
	public void init()
	{    
	   //Create Corpus and initiate processing to create the dictionary
	   setCorpus("gutenberg", true); 
	   initiateProcessing();
	   String hitsPath = corpusPath.substring(0, corpusPath.indexOf("gutenberg")); 
	   populateWordHits(hitsPath + "/wordHits.txt");
	}
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException{
		/*
		 * The HTTP GET requests must be of the form:
		 *  http://localhost:8080/CorpusServer/Count?word=SampleWord  
		 *  request parameter "word" is processed to extract the desired word
		 */
		String word = request.getParameter("word").toLowerCase();
		word = WordCountThread.preProcessWord(word);
		HttpSession session = request.getSession(); 
		PrintWriter out = response.getWriter();
		
		//Keep track of the hits for this particular word
		if(wordHits.containsKey(word))
		{
			Integer hits = wordHits.get(word);
			wordHits.put(word, hits+1);
		}
		else
		{
		    wordHits.put(word, 1);	
		}
		int wordAccessedCnt = 0;
		if((wordHits.get(word)!=null) && (wordHits.get(word)>0))
		{
			wordAccessedCnt = wordHits.get(word);
		}
		
		int wordCnt = 0;
		if((corpusDict.get(word)!=null) && (corpusDict.get(word)>0))
		{
			wordCnt = corpusDict.get(word);
		}
		
		//Print the Integers(number of requests, occurences of this word) as per specification
		 System.out.println(wordAccessedCnt);
		 System.out.println(wordCnt); 

		//Present the results in the form of HTML Page
		out.println("<html>");
		out.println("<body>"); 
		out.println("<h1> <center>" + word + "</center> </h1>");
		out.println("<h2> Hits: " + wordAccessedCnt + " </h2>");
		out.println("<h2> Corpus Frequency: " + wordCnt  + " </h2>");
		out.println("</body>");
		out.println("</html>");	
	} 
}

