package com.myproject.test;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import org.junit.Before;
import org.junit.Test;

import com.myproject.CountServerListener;

public class CountServerListenerTest {

    String hitsFilePath;
	
    @Before
    //Setup the corpus path
	public void setUp() 
    {
		String wholePath =  CorpusTest.class.getResource("").getPath(); 
		String projectPath = wholePath.substring(0, wholePath.indexOf("build")); 
		File testFolder = new File(projectPath + "/WebContent/WEB-INF/corpus/testHitsFolder/");
		hitsFilePath = testFolder.getAbsolutePath() + "/";  
	}
    
    @Test
    public void testRecordHits()
    {
    	CountServerListener listenerObj = new CountServerListener();
    	listenerObj.errorCode=0;
    	listenerObj.hitsFilePath = this.hitsFilePath; 
		Hashtable<String, Integer> testTable = new Hashtable<String, Integer>();
		testTable.put("testNegative", -3);
		listenerObj.recordHits(testTable);
		assertEquals("Error not thrown when trying to test negative value as key",1,listenerObj.errorCode);
    }
    
    @Test
    public void testRecordHitsNullFilePath()
    {
    	CountServerListener nullObj = new CountServerListener();
    	nullObj.errorCode = 0;
    	nullObj.hitsFilePath = null;
		Hashtable<String, Integer> testTable = new Hashtable<String, Integer>();
    	nullObj.recordHits(testTable);
    	assertEquals("Error not thrown when trying to test null file path", 1, nullObj.errorCode);
    }
    
	
    @Test
	public void testRecordHitsBasic() 
	{
		CountServerListener listenerObj = new CountServerListener();
		listenerObj.hitsFilePath = this.hitsFilePath; 
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(hitsFilePath + "wordHits.txt"))) {
		    String line;
		    bw.write("testKeyA-1");
		    bw.close(); 
		}
		catch(Exception ex)
		{
			System.out.println("Exception occurred while trying to write to wordHits.txt");
		    System.out.println(ex.toString());
		} 
		
		Hashtable<String, Integer> testTable = new Hashtable<String, Integer>();
		testTable.put("testKeyA", 20); //Updating the old value
		testTable.put("testKeyB", 15); //A new keyValue pair
		
		/*
		 * New Contents of the file must be
		 * testKeyA-20  
		 * testKeyB-15
		 */
		listenerObj.recordHits(testTable);
		List<String> keysList = new ArrayList<String>();
		testTable.clear(); //Clear table before populating new contents
		
		try (BufferedReader br = new BufferedReader(new FileReader(hitsFilePath + "wordHits.txt"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       String wordCountPair[] = line.trim().split("-");
		       keysList.add(wordCountPair[0]);
		       testTable.put(wordCountPair[0], Integer.valueOf(wordCountPair[1])); 
		    } 
		}
		catch(Exception ex)
		{
			System.out.println("Exception occurred while trying to read wordHits.txt file after updation");
		    System.out.println(ex.toString());
		} 
		assertEquals("Final Hash Table Size incorrect", 2, keysList.size());
		assertEquals("Expected Key: testKeyA missing", true, keysList.contains("testKeyA"));
	    assertEquals("Expected Key: testKeyB missing", true, keysList.contains("testKeyB"));
	    assertEquals("Value: testKeyA wrong", 20, (int)testTable.get("testKeyA"));
	    assertEquals("Value: testKeyB wrong", 15, (int)testTable.get("testKeyB")); 
	    System.out.println("Complete!");
	}
	
}
