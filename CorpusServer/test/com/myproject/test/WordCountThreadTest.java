package com.myproject.test;

import static org.junit.Assert.*;
import java.io.*;
import java.util.*;
import org.junit.*;
import com.myproject.WordCountThread;

public class WordCountThreadTest {

   String testCorpusPath;
	
    @Before
    //Setup the test corpus path
	public void setUp() {
		String wholePath =  CorpusTest.class.getResource("").getPath();
		String projectPath = wholePath.substring(0, wholePath.indexOf("build"));
		File testFolder = new File(projectPath + "/WebContent/WEB-INF/corpus/testCorpus");
		testCorpusPath = testFolder.getAbsolutePath(); 
	}
	 
	@Test
	//Test for the keys/values in the dictionary created by individual thread
	public void testRunCounts() {
		WordCountThread runObj = new WordCountThread(testCorpusPath + "/file1.txt");
		runObj.start();
		try {
		runObj.join();
		}
		catch(Exception ex)
		{
			fail("Thread Join failed while trying to test");
		}
		Hashtable<String, Integer> fileDict = runObj.getDictionary(); 
		
		assertEquals("File1 Dictionary Size is wrong",3,fileDict.size());
		
		Object[] keys = fileDict.keySet().toArray();
 		List<String> keyList =  (List<String>) (Object)Arrays.asList(keys);
 		//Test for all Keys
 		assertEquals("Key:they missing", true, keyList.contains("they"));  
 		assertEquals("Key:are missing", true, keyList.contains("are"));  
 		assertEquals("Key:here missing", true, keyList.contains("here"));   
 		
 		//Test for Values 
 		assertEquals("Value:they incorrect", 1, (int)fileDict.get("they"));  
 		assertEquals("Value:are incorrect", 5, (int)fileDict.get("are"));  
 		assertEquals("Value:here incorrect", 1, (int)fileDict.get("here"));  
	}
 
	//Test for the action of threads when a file does not exist
	@Test
	public void testRunInvalidFile()
	{
		WordCountThread runObj = new WordCountThread(testCorpusPath + "/file5.txt");
		Hashtable<String, Integer> fileDict = runObj.getDictionary(); 
		runObj.start();
		try {
		runObj.join();
		} catch(Exception ex)
		{
			fail("Thread did not end gracefully for invalid file");
		}
		assertEquals("Hashtable size non-zero for invalid file",0, fileDict.size());
	}
}
