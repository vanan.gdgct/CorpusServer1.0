package com.myproject.test;

import com.myproject.Corpus;
import com.myproject.WordCountThread; 
import static org.junit.Assert.*;
import java.io.File;
import java.util.*;
import org.junit.*;

//Dummy class for Code Injection
class LocalCorpus extends Corpus   
{

	public WordCountThread[] wc;
	public LocalCorpus(String corpusPath) {
		super(corpusPath);
		this.wc =  super.getThreads(); 
	}
	
	public void modifyThreads()
	{
		wc[0] = null; 
	}
	
}
public class CorpusTest{

	String testCorpusPath;
	
    @Before
    //Setup the corpus path
	public void setUp() {
		String wholePath =  CorpusTest.class.getResource("").getPath();
		String projectPath = wholePath.substring(0, wholePath.indexOf("build"));
		File testFolder = new File(projectPath + "/WebContent/WEB-INF/corpus/testCorpus");
		testCorpusPath = testFolder.getAbsolutePath(); 
	}
	 
    @Test
    //Test for the merged Corpus dictionary keys and values
	public void testProcessCorpus() {
		Corpus processObj = new Corpus(testCorpusPath);
		processObj.processCorpus(); 
		Hashtable<String, Integer> dictionary = processObj.getCorpusDict();
		assertEquals("Dictionary Size Incorrect", 10, dictionary.size());
		dictionary.keySet();
		System.out.println("Dictionary Size: " + dictionary.size());
		Object[] keys = dictionary.keySet().toArray();
 		List<String> keyList =  (List<String>) (Object)Arrays.asList(keys);
 		//Test for all Keys
 		assertEquals("Key:fruits missing", true, keyList.contains("fruits"));  
 		assertEquals("Key:apples missing", true, keyList.contains("apples"));  //Capitalized Word
 		assertEquals("Key:they missing", true, keyList.contains("they")); 
 		assertEquals("Key:are missing", true, keyList.contains("are")); 
 		assertEquals("Key:here missing", true, keyList.contains("here")); 
 		assertEquals("Key:he missing", true, keyList.contains("he")); 
 		assertEquals("Key:is missing", true, keyList.contains("is")); 
 		assertEquals("Key:these missing", true, keyList.contains("these")); 
 		assertEquals("Key:files missing", true, keyList.contains("files")); 
 		assertEquals("Key:hey missing", true, keyList.contains("hey")); //Punctuation discarded

 		
 		//Test for Values
 		//Fruits - #1 in file4.txt
 		assertEquals("Value:fruits incorrect", 1, (int)dictionary.get("fruits")); 
 		//Apples - #1 in file4.txt
 		assertEquals("Value:apples incorrect", 1, (int)dictionary.get("apples"));  
 		//They - #1 in file1.txt + #4 in file4.txt = 5
 		assertEquals("Value:they incorrect", 5, (int)dictionary.get("they"));  
 		//are - #5 in file1.txt + #2 in file4.txt + #1 in file3.txt = 8
 		assertEquals("Value:are incorrect", 8, (int)dictionary.get("are"));  
 		//here - #1 in file1.txt + #1 in file2.txt = 2
 		assertEquals("Value:here incorrect", 2, (int)dictionary.get("here")); 
 		//he - #1 in file2.txt  
 		assertEquals("Value:here incorrect", 1, (int)dictionary.get("he")); 
 		//is - #1 in file2.txt + #1 in file4.txt = 2
 		assertEquals("Value:here incorrect", 2, (int)dictionary.get("is")); 
 		//These - #1 in file3.txt
 		assertEquals("Value:here incorrect", 1, (int)dictionary.get("these")); 
 		//files- #1 in file3.txt
 		assertEquals("Value:here incorrect", 1, (int)dictionary.get("files")); 
 		//Hey!- #1 in file4.txt
 		assertEquals("Value:here incorrect", 1, (int)dictionary.get("hey"));  
 		System.out.println("Done all keys");
	}
	
	
	@Test
	//Test if the threads gracefully end when an error is encountered
	public void testProcessCorpusException()
	{
		LocalCorpus threadObj  = new LocalCorpus(this.testCorpusPath);
		threadObj.modifyThreads();
		threadObj.processCorpus();
		assertEquals("Error Not Thrown on Thread Join", 1, threadObj.getErrorCode());
		
	}
	 
	@Test
	//Test the mergeHashTables() function with variations of input hash tables
	public void testmergeHashTables(){
		Hashtable<String, Integer> table1 = new Hashtable<String, Integer>();
		Hashtable<String, Integer> table2 = new Hashtable<String, Integer>(); 
		Hashtable<String, Integer> result;
		Corpus mergeObj = new Corpus(testCorpusPath);
		
		List<Hashtable<String, Integer>> listTables = new ArrayList<Hashtable<String, Integer>>();
		//Test When a list containing no hash tables is given as input
		result = mergeObj.mergeHashTables(listTables);
		assertEquals( "No Hash Tables case fails",0, result.size()); 
		
		//Test when list consists of hashtables which are empty
		listTables.add(table1);
		listTables.add(table2);
		result = mergeObj.mergeHashTables(listTables);
		System.out.println("result size: " + result.size());
		assertEquals( "List consisting of empty hash tables fails", 0, result.size()); 
		
		//Test list having hashtable with one value
		table1.put("a", 1);
		listTables.clear();
		listTables.add(table1);
		//Merged HashTable must contain Key:a, Value: 1
		result = mergeObj.mergeHashTables(listTables);
		System.out.println("Result size: " + result.size());
		assertEquals("Size incorrect for hash table with 1 element",1,result.size());
		Object[] keys = result.keySet().toArray();
		for(Object k : keys)
		{
			System.out.println("Key is: " + k.toString());
		}
		System.out.println("Keys Length: " + keys.length);
		assertEquals("Key size incorrect for hash table with 1 element", 1, keys.length);
		assertEquals("Key incorrect for hash table with one element", "a", keys[0].toString());
		Integer val = result.get(keys[0].toString());
		assertEquals("Value incorrect for hash table with one element", 1, (int)result.get(keys[0]));
		
		//Test lists having hashtables with multiple values
		table2.put("a", 10);
		table2.put("b", 20);
		/* Merged HashTable must contain 
		 *  Key:a, Value: 1 (from Table 1) + 10 = 11
		 *  Key:b, Value: 20
		 */
         listTables.clear();
         listTables.add(table1);
         listTables.add(table2);
         result = mergeObj.mergeHashTables(listTables);
         assertEquals("Size incorrect for hash table with 1 element",2,result.size());
 		 keys = result.keySet().toArray();
 		 List<String> keyList =  (List<String>) (Object)Arrays.asList(keys);
 		 assertEquals("Key a not present in merged hash table", true, keyList.contains("a"));
 		 assertEquals("Key b not present in merged hash table", true, keyList.contains("b"));
 		 assertEquals("Value incorrect for Key a in merged hash table", 11, (int)result.get("a"));
 		 assertEquals("Value incorrect for Key b in merged hash table", 20, (int)result.get("b")); 
	}

}
