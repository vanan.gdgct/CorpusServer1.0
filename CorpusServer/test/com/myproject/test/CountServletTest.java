package com.myproject.test;
 
import static org.mockito.Mockito.when;
import java.io.*;
import java.util.*; 
import junit.framework.TestCase;
import org.junit.*;
import org.mockito.*;
import javax.servlet.http.*;
import com.myproject.*;
 
class CountServletDummy extends CountServlet
{ 
	@Override
	public void init()
	{    
		   setCorpus("testCorpus", false); 
		   initiateProcessing();
		   super.wordHits = new Hashtable<String, Integer>();
	}
}

class CountServletDummy2 extends CountServlet
{ 
	
	public Hashtable<String,Integer> gethashtable()
	{
		return super.wordHits;
	}
}

public class CountServletTest extends TestCase {

	 @Mock
	 HttpServletRequest request;
	 @Mock
	 HttpServletResponse response;
	 @Mock
	 HttpSession session; 

	 @Before
	 protected void setUp() throws Exception {
	  MockitoAnnotations.initMocks(this);
	 }
	 
	 @Test
	 public void testpopulateWord()
	 { 
	   try (BufferedWriter bw = new BufferedWriter(new FileWriter("test.txt"))) {
		    bw.write("a-1"); 
		    bw.close(); 
		}
		catch(Exception ex)
		{
			System.out.println("Exception occurred while trying to write to test.txt");
		    System.out.println(ex.toString());
		} 
	   CountServletDummy2 testObj = new CountServletDummy2();
	   testObj.populateWordHits(new File("test.txt").getAbsolutePath());
	   Hashtable<String, Integer> htable =  testObj.gethashtable();
	   assertEquals("Size not 1 for a-1 entry", 1, htable.size());
	   Object[] keys = htable.keySet().toArray();
	   List<String> keyList =  (List<String>) (Object)Arrays.asList(keys);
	   assertEquals("Key:a missing", true, keyList.contains("a"));
	   assertEquals("Value for a wrong", 1, (int)htable.get("a")); 
	   File testFile = new File("test.txt");
	   testFile.delete();
	 }
	 
	 @Test
	 public void testpopulateWordFileNotExisting()
	 { 
	   CountServletDummy2 testObj = new CountServletDummy2();
	   testObj.populateWordHits(new File("test.txt").getAbsolutePath());
	   Hashtable<String, Integer> htable =  testObj.gethashtable(); 
	   assertEquals("Error not thrown for non-existing files", 1, testObj.errorServlet); 
	 }
	 
	 //Test doGet method of the servlet with different words in the get requests
	 @Test
	 public void testdoGet() throws Exception {
	  //Setup initial parameters
	  when(request.getParameter("word")).thenReturn("apples"); 
	  when(request.getSession()).thenReturn(session); 
	  StringWriter sw = new StringWriter();
	  PrintWriter pw = new PrintWriter(sw);
	  when(response.getWriter()).thenReturn(pw);
	  
	  CountServletDummy testObj = new CountServletDummy();
	  testObj.init();
	  
	  //Pass a variety of doGet requests one after the other and record the responses
	  testObj.doGet(request, response); 
	  String buffer = sw.getBuffer().toString().trim();
	  String[] lines = buffer.split("\n");  
	  ArrayList<String> results = new ArrayList<String>();
	  
	  /* Expected
	   * <h3> Hits: 1 </h3>
	   * <h3> Corpus Frequency: 1 </h3>
	   */
	  for(String line : lines)
	  { 
		  if(line.contains("Hits"))   
			  results.add(line);
		  if(line.contains("Corpus Frequency")) 
			  results.add(line);
	  }
	    
	  sw = new StringWriter();
	  pw = new PrintWriter(sw);
	  when(response.getWriter()).thenReturn(pw);
	  testObj.doGet(request, response); 
	  buffer = sw.getBuffer().toString().trim();
	  lines = buffer.split("\n");  
	  
	  /* Expected
	   * <h3> Hits: 2 </h3>
	   * <h3> Corpus Frequency: 1 </h3>
	   */
	  for(String line : lines)
	  { 
		  if(line.contains("Hits"))
			  results.add(line);
		  if(line.contains("Corpus Frequency"))
			  results.add(line);
	  }
	  
	  
	  //Test Word Capitalized, but internally converted to lowercase and counts calculated
	  when(request.getParameter("word")).thenReturn("Are");  
	  sw = new StringWriter();
	  pw = new PrintWriter(sw);
	  when(response.getWriter()).thenReturn(pw);
	  testObj.doGet(request, response); 
	  buffer = sw.getBuffer().toString().trim();
	  lines = buffer.split("\n");    
	  
	  /* Expected
	   * <h3> Hits: 1 </h3>
	   * <h3> Corpus Frequency: 8 </h3>
	   */
	  for(String line : lines)
	  { 
		  if(line.contains("Hits"))
			  results.add(line);
		  if(line.contains("Corpus Frequency"))
			  results.add(line);
	  }
	  
	  
	  //Test Word Not Found
	  when(request.getParameter("word")).thenReturn("Vanan"); 
	  sw = new StringWriter();
	  pw = new PrintWriter(sw);
	  when(response.getWriter()).thenReturn(pw);
	  testObj.doGet(request, response); 
	  buffer = sw.getBuffer().toString().trim();
	  lines = buffer.split("\n");    
	  
	  /* Expected
	   * <h3> Hits: 1 </h3>
	   * <h3> Corpus Frequency: 8 </h3>
	   */
	  for(String line : lines)
	  { 
		  if(line.contains("Hits"))
			  results.add(line);
		  if(line.contains("Corpus Frequency"))
			  results.add(line);
	  }
	  
	  for(String s: results)
	  {
		  System.out.println("Line: " + s);
	  }
	  
	  //Check all the values recorded above
	  String[] words = results.get(0).split(" ");
	  assertEquals("Hits first Time for apples not 1", 1, (int)Integer.parseInt(words[2]));
	  
	  words = results.get(1).split(" " );
	  assertEquals("Corpus Frequency for apples incorrect second time", 1, (int)Integer.parseInt(words[3]));
	  
	  words = results.get(2).split(" " );
	  assertEquals("Hits second Time for apples not 2", 2, (int)Integer.parseInt(words[2]));
	  
	  words = results.get(3).split(" " );
	  assertEquals("Corpus Frequency for apples incorrect second time", 1, (int)Integer.parseInt(words[3]));
	  
	  words = results.get(4).split(" ");
	  assertEquals("Hits for are not 1", 1, (int)Integer.parseInt(words[2]));
	  
	  words = results.get(5).split(" " );
	  assertEquals("Corpus Frequency for are incorrect", 8, (int)Integer.parseInt(words[3]));
	   
	  words = results.get(6).split(" ");
	  assertEquals("Hits for Vanan not 1", 1, (int)Integer.parseInt(words[2]));
	  
	  words = results.get(7).split(" " );
	  assertEquals("Corpus Frequency for Vanan not non-zero", 0, (int)Integer.parseInt(words[3]));
	 } 
	 
}