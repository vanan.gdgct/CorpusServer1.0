
                            CorpusServer1.0 README
                            ______________________


RUNNING THE WEB-APP
——————————————————— 


METHOD 1: MANUALLY THROUGH WAR FILE
——————————————————————————————————— 

To run the App manually, perform the following - 

* Deploy CorpusServer.war provided with the package to the webapps location of the Tomcat-8.0.30 server (Default: /usr/local/apache-tomcat-8.0.30/webapps)

* Make sure that Tomcat Server is started by executing startup.sh; If its already started, Stop and Start it again so that the deployed war file gets activated

* Open any Web Browser of your choice and send a GET Request in the following format:
http://localhost:8080/CorpusServer/Count?word=A

The Output is displayed in the Web Page in the following format:

                       <Word>
Hits: <Number of Requests for this Word in the server)

Corpus Frequency: <Occurrences of this Word in the corpus)

*Perform shutdown.sh to stop the Server and repeat the process all over again to understand how the hits are getting carried over when the server functions with breaks.


METHOD 2: THROUGH ECLIPSE
————————————————————————— 

Unzip CorpusServer.zip into the Eclipse Workspace and open the project in Eclipse.

* CountServlet.java is the servlet which starts the process
* On the Servers tab, make sure Tomcat v8.0 is added, and start the server
* Go to Web Browser and perform the GET Request as mentioned in METHOD 1;
In addition to getting the Web Page as displayed above, the following comments explaining which step of the procedure the application is currently in is also displayed in the console:

Processing Corpus
Starting File Threads.. Done!
Waiting for File Threads to Complete.. 
Thread <Thread_ID> Finished..
All Threads Completed!
Creating the Dictionary…   Done!
<Number of Requests for this word in the Server>
<Occurrences of this Word in the corpus>


DEPENDENCIES
_____________
mockito-all-1.9.5.jar
mysql-connector-java.5.1.38-bin.jar



DEVELOPMENT ENVIRONMENT
_______________________
Apache Tomcat v8.0
Eclipse Version: Mars.1 Release (4.5.1)

